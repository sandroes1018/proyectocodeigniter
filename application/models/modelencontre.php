<?php 
class modelencontre extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	//creamos la funcion nuevo comentario que será la que haga la inserción a la base
	//de datos pasándole los datos a introducir en forma de array, siempre al estilo ci
	function nueva_placa($nombres,$apellidos,$telefono,$email,$placa,$ciudad,$tiempo,$franja,$placaimg,$fecha)
	{
		$data = array(
				'nombres' => $nombres,
				'apellidos' => $apellidos,
				'telefono' => $telefono,
				'email' => $email,
				'placa' => $placa,
				'ciudad_encontro' => $ciudad,
				'tiempo_encontro' => $tiempo,
				'franja_dia' => $franja,
				'imagen' => $placaimg,
				'fecha_registro' => $fecha
				);

		
		//aqui se realiza la inserción, si queremos devolver algo deberíamos usar delantre return
		//y en el controlador despues de $nueva_insercion poner lo que queremos hacer, redirigir,
		//envíar un email, en fin, lo que deseemos hacer
		$this->db->insert('placaencontrada',$data);
	}


function buscar_placa ($placa){


	$query = $this->db->join('ciudad', 'ciudad.id = placaencontrada.ciudad_encontro');
	$query = $this->db->where("placaencontrada.placa",$placa);
	$query = $this->db->get("placaencontrada");

	 if($query->num_rows() > 0 ){
				 
					return $query->result();

				}	else{
					$query= array("row" =>"noData" );
					return $query;
				}

 }


	function validar_placa ($placa){

	$query = $this->db->where("placaencontrada.placa",trim($placa));
	$query = $this->db->get("placaencontrada");
	
	return $query->result();

}

function listarCiudad(){

	$query = $this->db->get("ciudad");

				 if($query->num_rows() > 0 ){
				 
					return $query->result();

				}	else{
					$query= array("row" =>"noData" );
					return $query;
				}
}

}