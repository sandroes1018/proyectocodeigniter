	<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class moteroemergencia extends CI_Controller{

     function __construct()
    {
        parent::__construct();
      
       $this->load->model('ModelMoteroEmergencia');
    }
    

public function vistahome () {

  $this->load->view('header');
  $this->load->view('motero');
  $this->load->view('footer');

}

public function vistaform(){

	$this->load->view('header');
	$this->load->view('moteroform');
	$this->load->view('footer');

}

public function insertMoteroemergencia(){

	
	 $data = array(
                'NombresMotero' =>$this->input->post('nombre'),
                'Apellidos' =>$this->input->post('apellidos'), 
                'Telefono' =>$this->input->post('Telefono'), 
                'FechaNaciemiento' =>$this->input->post('Fecha'), 
                'Eps' =>$this->input->post('Eps'), 
                'Traumas' =>$this->input->post('Traumas'),
                'AlergiasMedicamentos' =>$this->input->post('Alergias'),
                'TipoSangre' =>$this->input->post('Sangre'),
                'CorreoElectronico' =>$this->input->post('Correo'), 
                'PlacaEmergencia'=> $this->input->post('Placa')
                );


	$last_id=$this->ModelMoteroEmergencia->insertMotero($data);

		

		if ($this->input->post('nombresemergencia2') != "") {
		# code...
				$data2=array('Nombres' =>$this->input->post('nombresemergencia'),
				 'Telefono1' =>$this->input->post('telefono1emergencia'),
				 'Telefono2' =>$this->input->post('telefono2emergencia'),
				 'Parentezco' =>$this->input->post('Parentezco'),
				 'idMoteroEmergencia' => $last_id
				 );
				
				$data3=array('Nombres' =>$this->input->post('nombresemergencia2'),
				 'Telefono1' =>$this->input->post('telefono1emergencia2'),
				 'Telefono2' =>$this->input->post('telefono2emergencia2'),
				 'Parentezco' =>$this->input->post('Parentezco2'),
				 'idMoteroEmergencia' => $last_id
				 );
	$this->insertContactosMoteroemergencia($data2,$data3);
		
	}else{
	$data2=array('Nombres' =>$this->input->post('nombresemergencia'),
				 'Telefono1' =>$this->input->post('telefono1emergencia'),
				 'Telefono2' =>$this->input->post('telefono2emergencia'),
				 'Parentezco' =>$this->input->post('Parentezco'),
				 'idMoteroEmergencia' => $last_id
				 );

	$data3=null;
	$this->insertContactosMoteroemergencia($data2,$data3);
	}


	$this->load->view('header');
	$this->load->view('sucessMoteroEmergencia');
	$this->load->view('footer');

	}
	 public function insertContactosMoteroemergencia($data2,$data3){

	 	$this->ModelMoteroEmergencia->insertContactoEmergencia($data2,$data3);

	 }

	 public function FindMotero(){


	$this->load->view('header');
	$this->load->view('FindMotero');
	$this->load->view('footer');

	
	 }

	 public function FindDB(){

	$result['result']=$this->ModelMoteroEmergencia->findMotero(trim($this->input->post('Placa')));


	if ($result['result']=='error') {
	$this->load->view('header');
	$this->load->view('findemergencia',$result);
	$this->load->view('footer');

	}else{
	$this->load->view('header');
	$this->load->view('findemergencia',$result);
	$this->load->view('footer');
	}
		
	 }
	 
}



