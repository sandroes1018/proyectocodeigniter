    <br>
  	<br>
  	<br>
  	<br>
   
               <div class="container">
                      <div class="row">

                       <div class="col-md-6 col-md-offset-3" >
                         <div class="panel panel-danger">
                              <div class="panel-heading">Aclaracion</div>
                              <div class="panel-body">
                                Los datos que se ingresaran al sistema se podran consultar por el numero de placa en caso de que se vea involucrado en un accidente. <br>
                                Los datos son con el fin de facilitar un contacto en caso de emergencia que vele por su bienestar, y datos que ayuden a cualquier trato que se le de a la hora de la atencion medica.
                                <br>
                                <br>

                          
                               <!-- <a class="btn btn-primary" href="../Clubes/formClubes">Registrar Club</a>-->
                                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Registrar Club</button>

                              </div>
                            </div>
                        
                      </div>
                  </div>


                      <!--MODAL PARA FORMULARIO AGREGAR CLUB-->

                      <!-- Trigger the modal with a button -->
                       

                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Registrar Club</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                   
                
  <form id="formulario" onSubmit="return false" class="form-horizontal"  method="post" enctype="multipart/form-data" >

             <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Nombre del Club</label>
                    <div class="col-lg-8">
                      <input id="nombreClub" type="Text" class="form-control"  name="nombreclub"
                             placeholder="Nombre del Club" >
                    </div>
               </div>

               <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Nombres del Lider</label>
                    <div class="col-lg-8">
                      <input type="Text" class="form-control"  name="nombrelider"
                             placeholder="Nombres del Lider" >
                    </div>
               </div>

               <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Telefono Lider</label>
                    <div class="col-lg-8">
                      <input type="Text" class="form-control"  name="tel"
                             placeholder="Telefono de contacto del Lider" >
                    </div>
               </div>

               <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Telefono Dos <small>(Opcional)</small></label>
                    <div class="col-lg-8">
                      <input type="Text" class="form-control"  name="teldos"
                             placeholder="Telefono dos Opcional " >
                    </div>
               </div>

                <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Correo Electronico</label>
                    <div class="col-lg-8">
                      <input type="email" class="form-control"  name="email"
                             placeholder="Correo Electronico" >
                    </div>
               </div>
                <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Seleccionar logo</label>
                    <div class="col-lg-8">
                      <span style="font-size: 15px;" class = "btn btn-info btn-sm glyphicon glyphicon-upload btn btn-default btn-file" > Seleccionar<input id="file" type = "file" name="logo" > <small id="nombreFile"></small>
                       </span>
                    </div>
               </div> 

               <script type="text/javascript">
                 document.getElementById('file').onchange = function () {
                    console.log(this.value);
                    document.getElementById('nombreFile').innerHTML = document.getElementById('file').files[0].name;
                    
                    }
               </script>

                  <!--<input id="file" type = "file" name="logo" >-->
            

               
                   <button type="submit" id="btn-ingresar" class="btn btn-success col-lg-12" onclick="testHoldon('sk-cube-grid');">Registrar Datos</button>

                     
         
      </form> 
      <br>
      <br>
      <div id="respuesta" class="alert alert-success text-center" role="alert"></div>
   
    
  </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div>

                          </div>
                        </div>

                      <!--FIN MODAL-->
        
        		<div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
        				
  <form class="form-horizontal" role="form" method="post" action="<?php base_url();?>../moteroemergencia/insertMoteroemergencia">

                  <div class="form-group">
                    <label for="nombre" class="col-lg-2 control-label">Nombres</label>
                    <div class="col-lg-10">
                      <input type="Text" class="form-control" id="nombre" name="nombre"
                             placeholder="Nombres" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="apellidos" class="col-lg-2 control-label">Apellidos</label>
                    <div class="col-lg-10">
                      <input type="Text" class="form-control" id="apellidos" name="apellidos"
                             placeholder="Apellidos" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="Telefono" class="col-lg-2 control-label">Telefono</label>
                    <div class="col-lg-10">
                      <input type="Text" class="form-control" id="Telefono" name="Telefono"
                             placeholder="Telefono" required="required">
                    </div>
                  </div>

                    <div class="form-group">
                    <label for="Fecha" class="col-lg-2 control-label">Fecha Nacimiento</label>
                    <div class="col-lg-10">
                      <input type="Date" class="form-control" id="Fecha" name="Fecha"
                             placeholder="Fecha Nacimientos" required="required">
                    </div>
                  </div>

                   <div class="form-group">
                    <label for="Eps" class="col-lg-2 control-label">Eps</label>
                    <div class="col-lg-10">
                      <input type="Text" class="form-control" id="Eps" name="Eps"
                             placeholder="Eps" required="required">
                    </div>
                  </div>

                    <div class="form-group">
                    <label for="Traumas" class="col-lg-2 control-label">Traumas</label>
                    <div class="col-lg-10">
                      <input type="Text" class="form-control" id="Traumas" name="Traumas"
                             placeholder="Traumas" required="required">
                    </div>
                  </div>

                    <div class="form-group">
                    <label for="Alergias" class="col-lg-2 control-label">Alergias Medicas</label>
                    <div class="col-lg-10">
                      <input type="Text" class="form-control" id="Alergias" name="Alergias"
                             placeholder="Alergias Medicas" required="required">
                    </div>
                  </div>

                  
                  <div class="form-group">
                    <label for="franja" class="col-lg-2 control-label">Tipo de Sangre</label>
                     <div class="col-lg-10">
                           <select class="form-control" name="Sangre" required="required">
                             <option>Seleccione una</option>
                              <option>O+</option>
                              <option>O-</option>
                              <option>A+</option>
                              <option>A-</option>
                              <option>B+</option>
                              <option>B-</option>
                              <option>AB+</option>
                              <option>AB-</option>
                           </select>
                            
                     </div>
                  </div>

                   <div class="form-group">
                    <label for="Correo" class="col-lg-2 control-label">Correo Electronico</label>
                    <div class="col-lg-10">
                      <input type="email" class="form-control" id="Correo" name="Correo"
                             placeholder="Correo Electronico" required="required">
                    </div>
                  </div>

                    <div class="form-group">
                    <label for="Placa" class="col-lg-2 control-label">Placa</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="Placa" name="Placa"
                             placeholder="Placa" required="required">
                    </div>
                  </div>

                  <div class="panel panel-warning">
                      <div class="panel-heading">¿ Perteneces a algun club de moteros ?   <button id="trueClub" type="button" class="btn btn-warning ">Si</button></div>
                            </div>

                              
                  <div id="club" class="form-group">
                    <label for="franja" class="col-lg-4 control-label">Seleccione el nombre del Club</label>
                     <div class="col-lg-6">
                           <select id="selectClub" class="form-control" name="Sangre" required="required">
                             
                            </select>
                            
                     </div>
                  </div>


                       
                         <div class="panel panel-info">
                      <div class="panel-heading">Datos de Contacto en Caso de Emergencia<small> (Minimo 1,Maximo 2,<strong>Recomendable Dos</strong>)</small></div>
                            </div>
                        
                   
                 


                    <div class="form-group">
                    <label for="Placa" class="col-lg-2 control-label">Nombres</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Nombres y Apellidos De contacto de emergencia"
                             name="nombresemergencia" required="required">
                    </div>
                  </div>

                   <div class="form-group">
                    <label for="Placa" class="col-lg-6 control-label">Numero de Telefono</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Telefono Principal"
                             name="telefono1emergencia" required="required">
                    </div>
                  </div>

                   <div class="form-group">
                    <label for="Placa" class="col-lg-6 control-label pull-left">Numero de Telefono Opcional</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Telefono Opcional"
                             name="telefono2emergencia" required="required">
                    </div>
                  </div>  

                    <div class="form-group">
                    <label for="Placa" class="col-lg-4 control-label pull-left">Parentezco</label>
                    <div class="col-lg-8">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Ejm: Mama,Papa,Hermano,Pareja,Amigo"
                             name="Parentezco" required="required">
                    </div>
                  </div>               

                  


                  <div id="Botonadd">
                  <div class="form-group">
                    <div class="col-lg-offset-9 ">
                    <button id="trueContacto" type="button" class="btn btn-primary ">Add otro Contacto</button>
                    </div>
                  </div>
                </div>


                  
                  
                <hr>
                  <div id="Contactoemergencia">
                       <div class="form-group">
                    <label for="Placa" class="col-lg-2 control-label">Nombres</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Nombres y Apellidos De contacto de emergencia"
                             name="nombresemergencia2">
                    </div>
                  </div>

                   <div class="form-group">
                    <label for="Placa" class="col-lg-6 control-label">Numero de Telefono</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Telefono Principal"
                             name="telefono1emergencia2">
                    </div>
                  </div>

                   <div class="form-group">
                    <label for="Placa" class="col-lg-6 control-label pull-left">Numero de Telefono Opcional</label>
                    <div class="col-lg-6">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Telefono Opcional"
                             name="telefono2emergencia2">
                    </div>
                  </div>  

                    <div class="form-group">
                    <label for="Placa" class="col-lg-4 control-label pull-left">Parentezco</label>
                    <div class="col-lg-8">
                      <input type="text" class="form-control" id="Placa" 
                             placeholder="Ejm: Mama,Papa,Hermano,Pareja,Amigo"
                             name="Parentezco2">
                    </div>
                  </div>          

                  </div>

                  <button id="btn-ingresar" class="btn btn-success btn-block"  type="submit" class="btn btn-success" >Registrar Datos</button>
                  

                </form>            
        						</div>        					
        				</div>
        		</div>
          </div>

  <script src="<?php echo base_url();?>asset/js/peticion_ajax.js"></script>

