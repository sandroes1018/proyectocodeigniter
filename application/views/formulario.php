	<br>
	<br>
	<br>
	<br>

   <div class="container">
                      <div class="row">

                       <div class="col-md-6 col-md-offset-3" >
                         <div class="panel panel-danger">
                              <div class="panel-heading">Formulario de registro de Placa Perdida </div>
                              <div class="panel-body">
                             Acontinuacion tendra un formulario donde debe ingresar algunos datos basico para que puedan comunicarsen con usted.
                              </div>
                            </div>
                        
                      </div>
                  </div> 
      				<div class="row">


      						<div class="col-md-6 col-md-offset-3">
      									
      					<form class="form-horizontal" action="<?php base_url();?>../registrar_placa/cargar_placa" method="post" enctype="multipart/form-data" >
                                                   
      							
      						<div class="form-group">
                            <label for="nombre" class="col-lg-2 control-label">Nombres</label>
                            <div class="col-lg-10">
                              <input type="text" class="form-control" id="nombre" name="nombre"
                                     placeholder="Nombres" required>
                            </div>
                      </div>

                           
                          
                           <div class="form-group">
                            <label for="apellidos" class="col-lg-2 control-label">Apellidos</label>
                            <div class="col-lg-10">
                              <input type="text" class="form-control" id="apellidos" name="apellidos" 
                                     placeholder="Apellidos" required>
                            </div>
                          </div>

                          
                       
                          <div class="form-group">
                            <label for="telefono" class="col-lg-2 control-label">Telefono</label>
                            <div class="col-lg-10">
                              <input type="text" class="form-control" id="telefono" name="telefono" 
                                     placeholder="Telefono" required>
                            </div>
                          </div>

                          

                            <div class="form-group">
                            <label for="email" class="col-lg-2 control-label">Correo electronico</label>
                            <div class="col-lg-10">
                              <input type="mail" class="form-control" id="email" name="email" 
                                     placeholder="Correo electronico" required>
                            </div>
                          </div>

                           
                            
                            <h4>Informacion de la placa</h4>
                            <br>

                            <div class="form-group">
                            <label for="placa" class="col-lg-2 control-label">Placa</label>
                            <div class="col-lg-10">
                            <input style="text-transform: uppercase;" type="text" class="form-control" id="placa" name="placa" 
                                    value="<?php echo $placa;?> " placeholder="Placa" required>
                            </div>
                          </div>

                         
                          
                         

                            <div class="form-group">
                            <label for="ciudad" class="col-lg-4 control-label">Ciudad en que la encontro ?</label>
                            <div class="col-lg-8">
                               <select id="selectCiudad" class="form-control" name="ciudad">                                    
                                         

                                      
                              </select>


                             <!-- <input type="text" class="form-control" id="ciudad" name="ciudad"                                    placeholder="Ejm: Bogota" required>-->
                            </div>
                          </div>

                            <div class="form-group">
                            <label for="tiempo" class="col-lg-4 control-label">Hace cuanto tiempo la encontro ?</label>
                            <div class="col-lg-8">
                            <select class="form-control" name="tiempo">
                                          <option>Menos de una semana</option>
                                          <option>La semana pasada</option>
                                          <option>El mes pasado</option>
                              </select>
                            <!--  <input type="text" class="form-control" id="tiempo" name="tiempo"                               
                                   placeholder="Ejm: la semana pasada" required > -->
                            </div>
                          </div>                         
                         

                             <div class="form-group">
                            <label for="franja" class="col-lg-4 control-label">Que franja del dia la encontro ?</label>
                            <div class="col-lg-8">
                             <select class="form-control" name="franja">
                                          <option>En la mañana</option>
                                          <option>En la tarde</option>
                                          <option>En la noche</option>
                              </select>
                            <!--  <input type="text" class="form-control" id="franja" name="franja"                                
                                     placeholder="Ejm: en la tarde" required> -->
                            </div>
                          </div>

                          
                  <div class="form-group">
                    <label for="nombre" class="col-lg-4 control-label">Foto de la placa</label> 
                    <div class="col-lg-8">
                      <span style="font-size: 15px;" class = "btn btn-info btn-sm glyphicon glyphicon-upload btn btn-default btn-file" > Seleccionar<input id="file" type = "file" name="placa" >  <small id="nombreFile"></small>
                       </span>
                    </div>
               </div> 

                          <script type="text/javascript">
                 document.getElementById('file').onchange = function () {
                    console.log(this.value);
                    document.getElementById('nombreFile').innerHTML = document.getElementById('file').files[0].name;
                    
                    }
               </script>

               <br>
               <br>  
                              <button type="submit" class="btn btn-success btn-block">Registrar</button>
      								</form> 

                      <?php   
echo "la fecha actual es " . date("d-m-Y"); ?>

      						</div>
      					
      				</div>
      			
      		</div>
         

<script src="<?php echo base_url();?>asset/js/formularioCiudades.js"></script>