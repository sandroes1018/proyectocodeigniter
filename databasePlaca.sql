-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: placa
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'BELLO'),(2,'SANANDRES'),(3,'APARTADO'),(4,'ARMENIA'),(5,'BARRANQUILLA'),(6,'BUCARAMANGA'),(8,'BOGOTA'),(9,'BUGA'),(10,'BUENAVENTURA'),(11,'CANDELARIA'),(12,'CARTAGO(VALLEDELCAUCA)'),(13,'CHINCHIN'),(14,'CAJICA,CUNDINAMARCA'),(15,'CALDAS(ANTIOQUIA)'),(16,'CALI'),(17,'CODAZZI'),(18,'COTA'),(19,'CARTAGENA'),(20,'CALOTO,CAUCA'),(21,'CUCUTA,NORTEDESANTANDER'),(22,'FUSAGASUGA'),(23,'CARMENDEVIBORAL'),(24,'DOSQUEBRADAS,RISARALDA'),(25,'DUITAMA'),(26,'BARRANCABERMEJA'),(27,'MEDELLIN'),(28,'FACATATIVA'),(29,'FLORIDABLANCA'),(30,'FLORENCIA'),(31,'FUNZA'),(32,'GACHANCIPA,CUNDINAMARCA'),(33,'GUARNE,ANTIOQUIA'),(34,'IBAGUE,TOLIMA'),(35,'IPIALES'),(36,'ITAGUI'),(37,'JURADO'),(38,'LETICIA(AMAZONAS)'),(39,'MAGDALENA'),(40,'MAICAO'),(41,'MEDELLIN'),(42,'MADRID'),(43,'MOSQUERA,CUNDINAMARCA'),(44,'MONTERIA,CORDOBA'),(45,'MANIZALES'),(46,'NARIÑO'),(47,'NEIVA'),(48,'PALMIRA,VALLE'),(49,'PASTO'),(50,'PIEDECUESTA,SANTANDER'),(51,'PEREIRA'),(52,'PITALITO'),(53,'PUERTOGAITAN'),(54,'QUINDIO'),(55,'RIOHACHA'),(56,'SABANETA'),(57,'SANTANDER'),(58,'SOGAMOSO'),(59,'SIBERIAVIABOGOTA'),(60,'SINCELEJO'),(61,'SANTAMARTA'),(62,'SOACHA'),(63,'SOLEDAD'),(64,'SOPO'),(65,'SANTANDERDEQUILICHAO,CAUCA'),(66,'TENJO'),(67,'TUNJA'),(68,'VALLEDUPAR,CESAR'),(69,'VILLAVICENCIO'),(70,'CHIA,CUNDINAMARCA'),(71,'YUMBO,VALLE'),(72,'ZONAFRANCABOGOTA'),(73,'ZIPAQUIRA');
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `placaencontrada`
--

DROP TABLE IF EXISTS `placaencontrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `placaencontrada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` text,
  `placa` varchar(100) NOT NULL,
  `ciudad_encontro` text,
  `tiempo_encontro` text,
  `franja_dia` text,
  `imagen` varchar(100) NOT NULL,
  `fecha_registro` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `placaencontrada`
--

LOCK TABLES `placaencontrada` WRITE;
/*!40000 ALTER TABLE `placaencontrada` DISABLE KEYS */;
INSERT INTO `placaencontrada` VALUES (22,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','mdi03c ','3','Menos de una semana','En la mañana','mdi03c','2017-06-08'),(23,'sandro estiven pineda','pineda serna','3118564047','malejarf25@hotmail.com','0','3','Menos de una semana','En la mañana','0','2017-06-08'),(24,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','a ','3','Menos de una semana','En la mañana','a','2017-06-08'),(25,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','img ','3','Menos de una semana','En la mañana','img','2017-06-08'),(26,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','img ','3','Menos de una semana','En la mañana','img','2017-06-08'),(27,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','img ','3','Menos de una semana','En la mañana','img','2017-06-08'),(28,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','img ','3','Menos de una semana','En la mañana','img','2017-06-08'),(29,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','img ','3','Menos de una semana','En la mañana','img','2017-06-08'),(30,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','img ','3','Menos de una semana','En la mañana','img','2017-06-08'),(31,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','00000 ','3','Menos de una semana','En la mañana','00000','2017-06-08'),(32,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','00000 ','3','Menos de una semana','En la mañana','00000','2017-06-08'),(33,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','00000 ','3','Menos de una semana','En la mañana','00000','2017-06-08'),(34,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','00000 ','3','Menos de una semana','En la mañana','00000','2017-06-08'),(35,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','00000 ','3','Menos de una semana','En la mañana','00000','2017-06-08'),(36,'sandro estiven pineda','pineda serna','3118564047','sandroes1018@hotmail.com','00000 ','3','Menos de una semana','En la mañana','00000','2017-06-08');
/*!40000 ALTER TABLE `placaencontrada` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-28  9:07:48
