<?php 
class ModelMoteroEmergencia extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	//creamos la funcion nuevo comentario que será la que haga la inserción a la base
	//de datos pasándole los datos a introducir en forma de array, siempre al estilo ci
	function insertMotero($data){
			
			$this->db->insert('moteroemergencia',$data);

			return $this->db->insert_id();
	}

		function insertClub($data){
			
			$this->db->insert('clubes',$data);

		return	$this->db->insert_id();
	}

	function insertContactoEmergencia($data2,$data3){

		if (!is_null($data3)) {
			# code...
			$this->db->insert('contactoemergencia',$data2);
			$this->db->insert('contactoemergencia',$data3);
		}else{
		$this->db->insert('contactoemergencia',$data2);
		}

	}

	function findMotero($placa){

				$query =$this->db->join('contactoemergencia', 'contactoemergencia.idMoteroEmergencia = moteroemergencia.id');
				$query = $this->db->where("moteroemergencia.PlacaEmergencia",$placa);
				$query = $this->db->get("moteroemergencia");

				 if($query->num_rows() > 0 ){
				 
					return $query->result();

				}	else{
					$query="error";
					return $query;
				}
	}

	function listarClubes(){

		$query = $this->db->get("clubes");

				 if($query->num_rows() > 0 ){
				 
					return $query->result();

				}	else{
					$query= array("row" =>"noData" );
					return $query;
				}
	}

}