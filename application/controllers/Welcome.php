<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	  function __construct()
    {
        parent::__construct();
      
       $this->load->model('ModelMoteroEmergencia');
    }

	public function index()
	{
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('modalRegistroPlaca');
		$this->load->view('footer');
	}

	public function prueba()
	{
		$this->load->view('header');
		$this->load->view('prueba');
		$this->load->view('footer');
	}

	public function nosotros(){

		$this->load->view('header');
		$this->load->view('Nosotros');
		$this->load->view('footer');
		
	}

	public function listarClubes(){

		$result['result']=$this->ModelMoteroEmergencia->listarClubes();

    	echo json_encode($result); 

	}

	public function Contacto(){

	
			
	}
}

